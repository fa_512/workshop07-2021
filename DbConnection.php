<?php

class DbConnection
{
    private $host;
    private $user;
    private $password;
    private $databaseName;
    public $activeConnection;

    function __constructor($host, $user, $password, $databaseName)
    {
        $this->host = $host;
        $this->user = $user;
        $this->password = $password;
        $this->databaseName = $databaseName;
        $this->activeConnection = null;
        
    }

    function getMySQLConnection()
    {

        if (!$this->activeConnection) {
            $this->activeConnection = new mysqli($this->host, $this->user, $this->password, $this->databaseName);
        }
        if ($this->activeConnection->connect_errno) {
            echo "Failed to connect MySQL: " . $this->activeConnection->connect_error;
            exit();
        }
        return $this->activeConnection;
    }
}

